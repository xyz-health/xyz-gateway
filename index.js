const express = require('express');
const app = express();

// parse json request body
app.use(express.json());

app.post('/data', (req, res) => {
    res.json({
        status: 'ok'
    });
});

app.get('*', (req, res) => {
    res.status(200).send('OK');
});

const PORT = 80;

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
